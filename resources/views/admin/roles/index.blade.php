@extends('admin.layouts.app')
@php
$stt = (($_GET['page'] ?? 1) - 1) * 5;
@endphp
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Roles</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
                            <li class="breadcrumb-item active">Roles</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card-header clearfix">
                            <a href="{{route('roles.create')}}" type="button" class="btn btn-primary float-left"><i
                                    class="fas fa-plus"></i> Add Role</a>
                        </div>
                        <!-- /.card -->
                        @if ($message = Session::get('message'))
                            <div class="alert alert-success">
                                <p class="text-white">{{ $message }}</p>
                            </div>
                        @endif
                            <!-- /.card-header -->
                        <div class="card-header" style="padding-left: 20%; background: #f3f8fa ">
                            <div class="col-md-10" style="margin-top: 5px; margin-bottom: 10px">
                                <form method="GET" class="m-md-3 d-flex" style="padding-right: 20%">
                                    <input type="search" name="name" value="{{ request('name') }}"
                                           class="form-control"
                                           placeholder="search ...">
                                    <button type="submit" class="btn btn-primary" style="margin-left: 7px">
                                        search
                                    </button>
                                </form>
                            </div>
                        </div>
                            <div class="card-body">
                                <table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th style="width: 5%">ID</th>
                                        <th style="width: 10%">Name</th>
                                        <th>Permission</th>
                                        <th style="width: 15%">Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($roles as $role)
                                        <tr>
                                            <td>{{++$stt}}</td>
                                            <td>{{$role->display_name}}</td>
                                            <td >
                                                <div>@foreach($role->permissions as $key=>$permission)
                                                    <span style="font-size: 14px" class="text-black">{{ $permission->display_name }}
                                                        {{ $key + 1 == $role->permissions->count() ? '.' : ',' }}
                                                    </span>
                                                @endforeach
                                                </div></td>
                                            <td>
                                                    @hasPermission('role_show')
                                                    <a href="{{route('roles.show', $role->id)}}"
                                                       class="badge bg-success"><i class="far fa-eye"></i></a>
                                                    @endhasPermission
                                                    @hasPermission('role_edit')
                                                    <a href="{{route('roles.edit', $role->id)}}"
                                                       class="badge bg-yellow"><i class="fas fa-edit"></i></a>
                                                    @endhasPermission
                                                    @hasPermission('role_delete')
                                                    <a  title="Delete" >
                                                        <button  data-id="{{$role->id}}"
                                                            type="submit" class="badge bg-danger dele-role-btn" onclick="">
                                                            <i class="fas fa-trash"></i>
                                                        </button>
                                                        <form id="delete-role-form-{{$role->id}}"
                                                            action="{{route('roles.destroy', $role->id)}}" method="POST">
                                                            @method('DELETE')
                                                            @csrf
                                                        </form>
                                                    </a>
                                                    @endhasPermission
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                <p>
                                    {{$roles->appends(request()->all())->Links()}}
                                </p>

                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            <!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
@endsection

