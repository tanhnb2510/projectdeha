<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModal">New Product</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" style="">
                <form id="addNewProductForm" data-action="{{route('products.store')}}"
                      method="POST"
                      accept-charset="UTF-8"
                      enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Name:</label>
                        <input type="text" name="name" class="form-control" id="name">
                        <span class="text-danger form-check errors rs-errors error-name"></span>
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Price:</label>
                        <input type="text" name="price" class="form-control" id="price">
                        <div class="text-danger form-check errors rs-errors error-price"></div>
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Description:</label>
                        <textarea type="text" name="description" class="form-control" id="description"></textarea>
                        <div class="text-danger form-check errors rs-errors error-description"></div>
                    </div>
                    <div class="form-group">
                        <div class="mb-3">
                            <label class="form-label">Category</label>
                            <select class="form-select" name="category_id[]" aria-label="Default select example">
                                <option selected value="">Select Category</option>
                                @foreach($categories as $category)
                                    <option value="{{ $category->id }}">{{ $category->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="text-danger form-check errors error-category"></div>
                    </div>
                    <div class="form-group">
                        <label class="form-label">Image</label>
                        <div class="form-row">
                            <div class="col-10">
                                <input name="image" id="image" type="file" style="" class="form-control image" >
                            </div>
                            <div class="col-2">
                                <button id="delete_button" class=" btn btn-secondary" type="reset" ><i class="fa fa-trash" style="width: 50px"></i></button>
                            </div>
                        </div>
                        <div class="show-image" >

                        </div>
                        <p>
                        @error('image')
                        <div class="link-danger">{{ $message }}</div>
                        @enderror
                        </p>
                        <div class="text-danger form-check errors error-image"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" id="btn-create-product" class="btn btn-primary">Create</button>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>
