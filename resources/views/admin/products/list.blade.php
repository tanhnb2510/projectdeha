@php
    $stt = (($_GET['page'] ?? 1) - 1) * 5;
@endphp
<div class="card-body">
    <table id="example2" class="table table-bordered table-hover">
        <thead>
        <tr>
            <th style="width: 5%">ID</th>
            <th style="width: 10%; height:10%">Image</th>
            <th>Name</th>
            <th>Category</th>
            <th>Price</th>
            <th>Description</th>
            <th style="width: 15%">Actions</th>
        </tr>
        </thead>
        <tbody>
        @foreach($products as $product)
            <tr>
                <th scope="row">{{++$stt}} </th>
                <td>
                    <img src="{{ asset('uploads/products/'.$product->image) }}" width="80px" alt="">
                </td>
                <td>{{$product->name}}</td>
                <td>
                    @foreach($product->categories as $category)
                        {{$category->name}}
                    @endforeach
                </td>
                <td>{{$product->price . " ". "VNĐ"}}</td>
                <td>{{$product->description}}</td>
                <td>
                    @hasPermission('product_show')
                    <button data-toggle="modal" data-target="#showProduct"
                            id="btn-open-detail"
                            data-action="{{route('products.show', $product->id)}}"
                            class="showProduct badge bg-yellow"><i class="far fa-eye"></i></button>
                    @endhasPermission
                    @hasPermission('product_edit')
                    <button data-toggle="modal" data-action="{{route('products.edit',$product->id)}}"
                            data-target="#exampleModalEdit"
                            class="editProduct badge bg-green" id="btn-open-edit"
                    ><i class="fas fa-edit"></i></button>
                    @endhasPermission
                    @hasPermission('product_delete')
                    <button type="button"
                            data-action="{{route('products.destroy',$product->id)}}"
                            class="badge bg-danger"
                            data-name-show="{{$product->name}}"
                            id="btn_delete"
                            data-id="{{$product->id}}">
                        <i class="fas fa-trash"></i>
                    </button>
                    @endhasPermission
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <p>
        {{$products->appends(request()->all())->Links()}}
    </p>
</div>
<!-- /.card-body -->
