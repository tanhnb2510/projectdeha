@extends('admin.layouts.app')
@php
    $stt = (($_GET['page'] ?? 1) - 1) * 5;
@endphp
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Products</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
                            <li class="breadcrumb-item active">Products</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card-header clearfix">
                            @hasPermission('product_create')
                            <button type="button" class="btn btn-primary btn-create-new-product" data-toggle="modal"
                                    data-target="#exampleModal" data-whatever="@getbootstrap">Create </button>
                            @include('admin.products.create')
                            @endhasPermission
                        </div>
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success">
                                <p class="text-white">{{ $message }}</p>
                            </div>
                        @endif
                        <div class="card">
                            <div class="card-header" style="padding-left: 10%; background: #f3f8fa">
                                <div class="col-md-10" style="margin-top: 5px; margin-bottom: 10px">
                                    <form method="GET" class="m-md-3 d-flex form-search" style="padding-right: 10%">
                                        <select name="category_id" class=" form-control col-md-2 mr-2"
                                                aria-label="Default select example" id="category_search" >
                                            <option value="" selected>Category</option>
                                            @foreach($categories as $category)
                                                <option {{ $category->id == request('category_id') ? 'selected' : '' }}
                                                        value="{{ $category->id }}">{{ $category->name }}</option>
                                            @endforeach
                                        </select>
                                        <input type="search" name="name" class="form-control " id="name-search"
                                               placeholder="name ..."  value="{{$_GET['name'] ?? ''}}"
                                               style="margin-left: 7px">
                                        <input type="search" name="min"class="form-control min-price-search"
                                               placeholder="Min"  value="{{$_GET['min'] ?? ''}}"
                                               style="margin-left: 7px ;width: 25% " >
                                        <input type="search" name="max" class="form-control max-price-search"
                                               placeholder="Max" value="{{$_GET['max'] ?? ''}}"
                                               style="margin-left: 7px ;width: 25%" >
                                        <button id="btn-search" type="submit" class="btn btn-primary" style="margin-left: 7px">
                                            search
                                        </button>
                                    </form>
                                </div>
                            </div>
                            <div class="card">
                                <div id="list" data-action="{{route('products.list')}}">

                                </div>
                            </div>
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.Modal edit -->
    <div class="modal fade" id="exampleModalEdit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content" style="">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModal">Update Product</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div id="edit-product"></div>
                </div>
            </div>
        </div>
    </div>

    <!-- /.Modal detail -->
    <div class="modal fade" id="showProduct" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Show Product</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <section class="content">
                            <div id="detail-product"></div>
                    </section>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>


    {{--    <!-- /.content-wrapper -->--}}
@endsection
@push('js')
    <script src="{{asset('js/product.js')}}"></script>
@endpush
