@extends('admin.layouts.app')
@php
$stt = (($_GET['page'] ?? 1) - 1) * 5
@endphp
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Users</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
                            <li class="breadcrumb-item active">Users</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card-header clearfix">
                            @hasPermission('user_create')
                            <a href="{{route('users.create')}}" type="button" class="btn btn-primary float-left"><i
                                    class="fas fa-plus"></i> Add User</a>
                            @endhasPermission
                        </div>

                        @if ($message = Session::get('message'))
                            <div class="alert alert-success">
                                <p class="text-white">{{ $message }}</p>
                            </div>
                        @endif
                    <!-- /.card -->
                        <div class="card">
                            <div class="card-header" style="padding-left: 20%; background: #f3f8fa ">
                                <div class="col-md-10" style="margin-top: 5px; margin-bottom: 10px">
                                    <form method="GET" class="m-md-3 d-flex" style="padding-right: 20%">
                                        <select name="role_id" class=" form-control col-md-2 mr-2"
                                                aria-label="Default select example" style="width: 30%">
                                            <option value=" " selected>Roles</option>
                                            @foreach($roles as $role)
                                                <option {{ $role->id == request('role_id') ? 'selected' : '' }}
                                                        value="{{ $role->id }}">{{ $role->name }}</option>
                                            @endforeach
                                        </select>
                                        <input type="search" name="name"
                                               class="form-control"
                                               value="{{request('name')}}"
                                               placeholder="name ...">
                                        <input type="search" name="email"
                                               class="form-control"
                                               value="{{request('email')}}"
                                               placeholder="email ..." style="margin-left: 7px">
                                        <button type="submit" class="btn btn-primary" style="margin-left: 7px">
                                            search
                                        </button>
                                    </form>
                                </div>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th style="width: 3%">ID</th>
                                        <th style="width: 15%">Name</th>
                                        <th style="width: 20%">Email</th>
                                        <th style="width: 5%">Gender</th>
                                        <th style="width: 10%" >Roles</th>
                                        <th style="width: 10%">Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($users as $user)
                                        <tr>
                                            <td>{{++$stt}}</td>
                                            <td>{{$user->name}}</td>
                                            <td>{{$user->email}}</td>
                                            <td>
                                                {{ $user->gender == 0 ? 'male' : 'female' }}
                                            </td>
                                            <td>
                                                @foreach($user->roles as $role)
                                                    <span style="font-size: 12px"
                                                          class="badge badge-success">{{ $role->display_name }}</span>
                                                @endforeach
                                            </td>
                                            <td>

                                                    @hasPermission('user_show')
                                                    <a title="Show" href="{{route('users.show', $user->id)}}"
                                                       class="badge bg-success"><i class="far fa-eye"></i></a>
                                                    @endhasPermission
                                                    @hasPermission('user_edit')
                                                    @if(auth()->user()->id != $user->id)
                                                    <a title="Edit" href="{{route('users.edit', $user->id)}}"
                                                       class="badge bg-yellow"><i class="fas fa-edit"></i>
                                                    </a>
                                                    @endif
                                                    @endhasPermission
                                                    @hasPermission('user_delete')
                                                    @if(auth()->user()->id != $user->id)
                                                    <a  title="Delete">
                                                        <button type="submit" class="dele-user-btn badge bg-danger"
                                                        data-id="{{$user->id}}">
                                                            <i class="fas fa-trash"></i>
                                                        </button>
                                                        <form id="delete-user-form-{{$user->id}}" action="{{route('users.destroy', $user->id)}}" method="POST">
                                                        @method('DELETE')
                                                        @csrf
                                                        </form>
                                                    </a>
{{--                                                    <button data-id="{{$user->id}}"--}}
{{--                                                            class=" deleteUser badge bg-danger btn-delete"  data-name-show="{{$user->name}}"--}}
{{--                                                            data-action="{{route('users.destroy',$user->id)}}">--}}
{{--                                                        <i class="fas fa-trash"></i>--}}
{{--                                                    </button>--}}
                                                    @endif
                                                    @endhasPermission

                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                    {{$users->appends(request()->all())->Links()}}
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
@endsection
