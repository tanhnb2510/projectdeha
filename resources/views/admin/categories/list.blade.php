@extends('admin.layouts.app')
@php
    $stt = (($_GET['page'] ?? 1) - 1) * 5;
@endphp
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Categories</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
                            <li class="breadcrumb-item active">Category</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>
        <!-- Main content -->
        <section class="content" id="list">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <!-- /.card -->
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success">
                                <p class="text-white">{{ $message }}</p>
                            </div>
                        @endif
                        <div class="card-header clearfix">
                            <a style="margin-right: 10px" href="{{route('categories.create')}}" type="button" class="btn btn-primary float-left"><i
                                    class="fas fa-plus"></i> Add Category</a>
                        </div>
                        <div class="card">
                            <!-- /.card-header -->
                            <div class="card-body">
                                <table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th style="width: 5%">ID</th>
                                        <th>Name</th>
                                        <th>Parent Name</th>
                                        <th style="width: 15%">Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($data as $category)
                                        <tr id="xid{{$category->id}}">
                                            <td>{{++$stt}}</td>
                                            <td>{{$category->name}}</td>
                                            <td>{{ $category->parent->name ?? ""}}</td>
                                            <td>
                                                @hasPermission('category_edit')
                                                <a href="{{route('categories.edit', $category->id)}}"
                                                   class="badge bg-yellow"><i class="fas fa-edit"></i></a>
                                                @endhasPermission
                                                <a>
                                                    <button data-id="{{$category->id}}"
                                                            class=" deleteCategory badge bg-danger btn-delete"  data-name-show="{{$category->name}}"
                                                            data-action="{{route('categories.destroy',$category->id)}}">
                                                        <i class="fas fa-trash"></i>
                                                    </button>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                <p>
                                    {{$data->appends(request()->all())->Links()}}
                                </p>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
@endsection

