@extends('admin.layouts.app')
@php
    $stt = (($_GET['page'] ?? 1) - 1) * 5;
@endphp
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Category</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
                            <li class="breadcrumb-item active">Category</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card-header clearfix">
                            <a style="margin-right: 10px" href="{{route('categories.create')}}" type="button" class="btn btn-primary float-left"><i
                                    class="fas fa-plus"></i> Add Category</a>
                            <a href="{{route('categories.list')}}" type="button" class="btn btn-primary float-left"><i
                                    class="fas fa-list"></i> List Category</a>
                        </div>
                        <!-- /.card -->
                        @if ($message = Session::get('message'))
                            <div class="alert alert-success">
                                <p class="text-white">{{ $message }}</p>
                            </div>
                        @endif
                        <div class="card">
                            <!-- /.card-header -->
                            <div class="card-body">
                                <table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th style="width: 5%">ID</th>
                                        <th style="width: 10%">Name</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($categories as $key => $category)
                                        @if($category->parent_id == null)
                                        <tr id="xid">
                                            <td>{{++$stt}}</td>
                                            <td>{{$category->name}}</td>
                                        </tr>
                                        @endif
                                    @endforeach
                                    </tbody>
                                </table>
                                <p>
                                </p>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
@endsection

