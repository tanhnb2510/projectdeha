@extends('admin.layouts.app')

{{--@section('title', 'Dashboard')--}}
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper" id="something">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Categories</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
                            <li class="breadcrumb-item active">Categories</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card-header clearfix">
                            @hasPermission('category_create')
                            <button type="button" class="btn btn-primary" id="btn-add-category"  data-bs-toggle="modal"
                                    data-bs-target="#CreateCategoryModal">
                                <i class="fas fa-plus"></i> &nbsp; Add Category
                            </button>
                            <button type="button" class="btn btn-primary" id="btn-add-category"  data-bs-toggle="modal"
                                    data-bs-target="#CreateCategoryModal">
                                <i class="fas fa-plus"></i> &nbsp; List Category
                            </button>
                            @endhasPermission
                        </div>

                        @if ($message = Session::get('success'))
                            <div class="alert alert-success">
                                <p class="text-white">{{ $message }}</p>
                            </div>
                        @endif

                        <div class="card">
                            <div class="card-header" style="padding-left: 20%; background: #f3f8fa">
                                <div class="col-md-10" style="margin-top: 5px; margin-bottom: 10px">
                                    <form method="GET" class="m-md-3 d-flex" style="padding-right: 10%" id="search">
                                        <select name="parent_id" id="parent_id"  class=" form-control col-md-2 mr-2"
                                                aria-label="Default select example" style="width: 30%">
                                            <option value=" " selected>Parent Name</option>
                                            {{--                                            @foreach($categories as $category)--}}
                                            {{--                                                <option value="{{$parent->id}}">{{$parentId->name}}</option>--}}
                                            {{--                                            @endforeach--}}
                                            @foreach($categories as $key => $category)
                                                @if($category->parent_id == null)
                                                    <option value="{{$category->id}}">
                                                        {{$category->name}}
                                                    </option>
                                                @endif
                                            @endforeach

                                        </select>
                                        <input type="search" name="name" id="name" value="{{ request('name') }}"
                                               class="form-control"
                                               placeholder="name ...">
                                    </form>
                                </div>
                            </div>
                            <div id="list" data-action="{{route('categories.list')}}">
                            </div>
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->


    <!-- Create Modal -->
    <div class="modal" id="CreateCategoryModal">
        <div class="modal-dialog">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Create Category</h4>
                    <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
                </div>

                <form id="CreateCategoryForm" method="POST" enctype="multipart/form-data" data-action="{{route('categories.store')}}">
                    @csrf
                    <div class="modal-body">
                        <ul class="alert alert-warning d-none" id="_error_create"></ul>
                        <div class="form-group mb-3">
                            <label>Name</label>
                            <input type="text" class="form-control" id="nameCreate" name="name">
                            <span class="text-danger name_error_create"></span>
                        </div>
                        <div class="form-group mb-3">
                            <label>Parent Category</label>
                            <select class="form-control" id="parentIdCreate" name="parent_id">
                                <option value="">Chọn danh mục</option>
                                @foreach($categories as $parentId)
                                    <option value="{{$parentId->id}}">{{$parentId->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal"
                        >Close
                        </button>
                        <button type="button" class="btn btn-primary btn-create" id="btn-store"
                        >Save
                        </button>
                    </div>
                </form>

            </div>
        </div>
    </div>

    <!-- Edit Modal -->
    <div class="modal" id="EditCategoryModal">
        <div class="modal-dialog">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Update Category</h4>
                    <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
                </div>

                <div id="edit"></div>

            </div>
        </div>
    </div>

@endsection

