<?php

namespace App\Repositories;

abstract class BaseRepository
{
    public $model;

    public function __construct()
    {
        $this->model = app($this->model());
    }

    abstract public function model();

    public function all()
    {
        return $this->model->all();
    }

    public function create($dataCreate)
    {
        return $this->model->create($dataCreate);
    }

    public function find($id, $columns = ['*'])
    {
        return $this->model->findOrFail($id, $columns);
    }

    public function delete($id)
    {
        return $this->model->delete($id);
    }

    public function count()
    {
        return $this->model->all()->count();
    }
}
