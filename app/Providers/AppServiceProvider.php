<?php

namespace App\Providers;

use App\Services\UserService;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Blade;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */

    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Paginator::useBootstrapFive();

        Blade::if('hasPermission', function ($permission) {
            return auth()->user()->hasPermission($permission) || auth()->user()->isSuperAdmin();
        });

        Blade::if('hasRole', function ($role) {
            return auth()->user()->hasRole($role);
        });

        View::composer('*','App\ViewCompose\CategoryCompose');
        View::composer('*','App\ViewCompose\RoleComposer');
        View::composer('*','App\ViewCompose\UserComposer');
        View::composer('*', 'App\ViewCompose\ProductCompose');
    }
}
