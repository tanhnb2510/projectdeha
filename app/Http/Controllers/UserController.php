<?php

namespace App\Http\Controllers;

use App\Http\Requests\UpdateUserRequest;
use App\Http\Requests\UserRequest;
use App\Services\RoleService;
use App\Services\UserService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class UserController extends Controller
{
    protected $roleService;

    protected $userService;

    public function __construct(RoleService $roleService, UserService $userService)
    {
        $this->roleService = $roleService;
        $this->userService = $userService;
        view::share('roles',$this->roleService->all());
    }

    public function index(Request $request)
    {
        $users = $this->userService->search($request);

        return view('admin.users.index',compact('users'));
    }

    public function create()
    {
        return view('admin.users.create');
    }

    public function store(UserRequest $request)
    {
        $this->userService->create($request);
        return redirect(route('users.index'))->with('message','User created successfully ');
    }

    public function show($id)
    {
        $user = $this->userService->find($id);
        return view('admin.users.show',compact('user'));
    }

    public function edit($id)
    {
        $user = $this->userService->find($id);
        return view('admin.users.edit',compact('user'));
    }

    public function update(UpdateUserRequest $request, $id)
    {
        $this->userService->update($request, $id);

        return redirect()->route('users.index')->with('message', 'User updated successfully');
    }

    public function destroy($id)
    {
        $this->userService->destroy($id);
        return redirect(route('users.index'))->with('message', 'User delete successfully');
    }
}
