<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductRequest;
use App\Http\Resources\ProductResource;
use App\Services\CategoryService;
use App\Services\ProductService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class ProductController extends Controller
{
    protected $productService;

    protected $categoryservice;

    public function __construct(ProductService $productService, CategoryService $categoryService)
    {
        $this->productService = $productService;
        $this->categoryservice = $categoryService;
    }

    public function index()
    {
        return view('admin.products.index');
    }

    public function list(Request $request)
    {
        $products = $this->productService->search($request);
        return view('admin.products.list', compact('products'))->render();
    }

    public function create()
    {
        return view('admin.products.create');
    }

    public function store(ProductRequest $request)
    {
        $data = $this->productService->create($request);
        $productResource = new ProductResource($data);
        return $this->sendSuccessResponse($productResource, $message = 'create success', $status = 200);
    }

    public function show($id)
    {
        $product = $this->productService->find($id);
        return view('admin.products.show', compact('product'));
    }

    public function edit($id)
    {
        $product = $this->productService->find($id);
        return view('admin.products.edit', compact('product'));
    }

    public function update(ProductRequest $request, $id)
    {
        $data = $this->productService->update($request, $id);
        $productResource = new ProductResource($data);
        return $this->sendSuccessResponse($productResource, $message = 'edit success', $status = 200);
    }

    public function destroy($id)
    {
        $this->productService->delete($id);

        return $this->sendSuccessResponse('', 'Delete product success', $status = 200);
    }
}
