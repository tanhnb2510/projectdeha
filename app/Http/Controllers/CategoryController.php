<?php

namespace App\Http\Controllers;

use App\Http\Requests\CategoryRequest;
use App\Services\CategoryService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class CategoryController extends Controller
{
    protected $categoryService;

    public function __construct(CategoryService $categoryService)
    {
        $this->categoryService = $categoryService;
    }

    public function index(Request $request)
    {
        $data = $this->categoryService->search($request);

        return view('admin.categories.list', compact('data'));
    }

//    public function list(Request $request)
//    {
//        $data = $this->categoryService->search($request);
//
//        return view('admin.categories.list', compact('data'));
//    }

    public function create()
    {
        return view('admin.categories.create');
    }

    public function store(CategoryRequest $request)
    {
        $this->categoryService->create($request);
        return redirect()->route('categories.index')->with('message', 'Category Create successfully');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $data = $this->categoryService->find($id);
        return view('admin.categories.edit',compact('data'));
    }

    public function update(CategoryRequest $request, $id)
    {
        $this->categoryService->update($request,$id);
        return redirect()->route('categories.index')->with('message', 'Category updated successfully');
    }

    public function destroy($id)
    {
        $this->categoryService->delete($id);
        return $this->sendSuccessResponse('', 'Delete product success', $status = 200);
    }
}
