<?php

namespace App\Http\Controllers;

use App\Http\Requests\RoleRequest;
use App\Repositories\PermissionRepository;
use App\Services\PermissionService;
use App\Services\RoleService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class RoleController extends Controller
{
    protected $roleService;

    public function __construct(RoleService $roleService, PermissionService $permissionService)
    {
        $this->roleService = $roleService;
        $this->permissionService = $permissionService;
    }

    public function index(Request $request)
    {
        $roles = $this->roleService->search($request);
        return view('admin.roles.index', compact('roles'));
    }

    public function create()
    {
        return view('admin.roles.create');
    }

    public function store(RoleRequest $request)
    {
        $this->roleService->create($request);

        return redirect(route('roles.index'))->with('message', 'Roles created successfully');
    }

    public function show($id)
    {
        $role = $this->roleService->find($id);
        return view('admin.roles.show', compact('role'));
    }

    public function edit($id)
    {
        $role = $this->roleService->find($id);
        return view('admin.roles.edit', compact('role'));
    }

    public function update(RoleRequest $request, $id)
    {
        $this->roleService->update($request, $id);

        return redirect(route('roles.index'))->with('message', 'Roles updated successfully');
    }

    public function destroy($id)
    {
        $this->roleService->delete($id);

        return redirect(route('roles.index'))->with('message', 'Roles deleted successfully');
    }
}
