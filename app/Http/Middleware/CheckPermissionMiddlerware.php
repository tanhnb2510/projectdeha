<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class CheckPermissionMiddlerware
{
    public function handle(Request $request, Closure $next , $permission)
    {
        if(auth()->user()->hasPermission($permission) || auth()->user()->isSuperAdmin())
        {
            return $next($request);
        }
        return abort(403);
    }
}
