<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class CheckRole
{
    public function handle(Request $request, Closure $next ,$role)
    {
        if (auth()->user()->hasRole($role) || auth()->user()->isSuperAdmin())
        {
            return $next($request);
        }
        return abort(403);
    }
}
