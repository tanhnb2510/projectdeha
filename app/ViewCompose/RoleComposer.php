<?php

namespace App\ViewCompose;

use App\Services\PermissionService;
use App\Services\RoleService;
use Illuminate\Support\Facades\View;

class RoleComposer
{
    protected $roleService;

    public function __construct(RoleService $roleService, PermissionService $permissionService)
    {
        $this->roleService = $roleService;
        $this->permissionService = $permissionService;
    }

    public function compose(\Illuminate\View\View $view)
    {
        $view->with('permissions',$this->permissionService->all());
        $view->with('permissionGroup', $this->permissionService->getWithGroup());
        $view->with('roleCount',$this->roleService->count());
    }
}
