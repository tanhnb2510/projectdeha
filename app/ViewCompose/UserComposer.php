<?php

namespace App\ViewCompose;

use App\Services\UserService;
use Illuminate\View\View;

class UserComposer
{
    protected $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function compose(View $view)
    {
        $view->with('usercount',$this->userService->count());
    }
}
