<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    protected $fillable = [
        'name',
        'email',
        'password',
        'birthday',
        'address',
        'phone_number',
        'gender',
    ];

    protected $hidden = [
        'password',
        'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function roles()
    {
        return $this->belongsToMany(
            Role::class,
            'role_user',
            'user_id',
            'role_id'
        );
    }

    public function scopeWithName($query, $name)
    {
        return $name ? $query->where('name', 'like', '%' . $name . '%') : null;
    }

    public function scopeWithEmail($query, $email)
    {
        return $email ? $query->where('email', $email ) : null;
    }

    public function scopeWithRoleId($query, $roleId)
    {
//        return $roleId ? $query->WhereHas('roles', function ($role) use ($roleId) {
//            $role->where('role_id', $roleId);
//        }) : null;

        return $roleId ? $query->WhereHas('roles', fn($role)=>$role->where('role_id', $roleId)) : null;
    }

    public function syncRole($roleId)
    {
        return $this->roles()->sync($roleId);
    }

    public function attachRole($roleId)
    {
        return $this->roles()->attach($roleId);
    }

    public function hasPermission($permission)
    {
        foreach ($this->roles as $role) {
            if ($role->hasPermission($permission)) {
                return true;
            }
        }
        return false;
    }

    public function hasRole($role)
    {
        return $this->roles->contains('name', $role);
    }

    public function isSuperAdmin()
    {
        return $this->hasRole('super_admin');
    }

    public function detachRole()
    {
        return $this->roles()->detach();
    }
}
