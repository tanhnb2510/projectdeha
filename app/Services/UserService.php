<?php

namespace App\Services;

use App\Repositories\UserRepository;
use http\Exception\InvalidArgumentException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;

class UserService
{
    protected $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function create($request)
    {
        $dataCreate = $request->all();
        $dataCreate['password'] = Hash::make($request->password);
        $dataCreate['role_name'] = $request->role_name ?? [];
        $user = $this->userRepository->create($dataCreate);

        $user->attachRole($dataCreate['role_name']);
        return $user;
    }


    public function search($request)
    {
        $dataSearch = $request->all();
        $dataSearch['name'] = $request->name ?? '';
        $dataSearch['email'] = $request->email ?? '';
        $dataSearch['role_id'] = $request->role_id ?? '';

        return $this->userRepository->search($dataSearch);
    }

    public function all()
    {
        return $this->userRepository->all();
    }

    public function find($id)
    {
        return $this->userRepository->find($id);
    }

    public function store($request)
    {
        $dataCreate = $request->all();
        $dataCreate['password'] = Hash::make($request->password);
        $user = $this->userRepository->create($dataCreate);

        $user->attachRole($request->role_name);
        return $user;
    }

    public function update($request, $id)
    {
        $user = $this->userRepository->find($id);
        $dataUpdate = $request->all();
        if ($request->password) {
            $dataUpdate['password'] = Hash::make($request->password);
        }
        $dataUpdate['role_name'] = $request->role_name ?? [];
        $user->update($dataUpdate);
        $user->syncRole($dataUpdate['role_name']);
        return $user;
    }

    public function destroy($id)
    {
        $user = $this->userRepository->find($id);

        $user->detachRole();
        $user->delete();
        return $user;
    }

    public function count()
    {
        return $this->userRepository->count();
    }
}
