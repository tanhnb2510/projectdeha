<?php

namespace App\Services;

use App\Repositories\RoleReposity;

class RoleService
{
    protected $roleRepository;

    public function __construct(RoleReposity $roleRepository)
    {
        $this->roleRepository = $roleRepository;
    }

    public function all()
    {
        return $this->roleRepository->all();
    }

    public function search($request)
    {
        $dataSearch = $request->all();
        $dataSearch['name'] = $request->name ?? '';

        return $this->roleRepository->search($dataSearch)->appends($request->all());
    }

    public function create($request)
    {
        $dataCreate = $request->all();
        $dataCreate['permission_name'] = $request->permission_name ?? [];
        $role = $this->roleRepository->create($dataCreate);

        $role->attachPermission($dataCreate['permission_name']);
        return $role;
    }

    public function find($id)
    {
        return $this->roleRepository->find($id);
    }

    public function delete($id)
    {
        $role = $this->roleRepository->find($id);

        $role->detachPermission();
        $role->delete();
        return $role;
    }

    public function update($request, $id)
    {
        $role = $this->roleRepository->find($id);
        $dataUpdate = $request->except('password');
        $dataCreate['permission_name'] = $request->permission_name ?? [];

        $role->update($dataUpdate);
        $role->syncPermission($dataCreate['permission_name']);
        return $role;
    }

    public function count()
    {
        return $this->roleRepository->count();
    }

}
