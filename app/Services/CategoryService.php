<?php

namespace App\Services;

use App\Repositories\CategoryRepository;

class CategoryService
{
    protected $categoryRepository;

    public function __construct(CategoryRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    public function search($request)
    {
        $dataSearch = $request->all();
        $dataSearch['name'] = $request->name ?? '';

        return $this->categoryRepository->search($dataSearch);
    }

    public function all()
    {
        return $this->categoryRepository->all();
    }

    public function create($request)
    {
        $dataCreate = $request->all();

        return $this->categoryRepository->create($dataCreate);
    }

    public function find($id)
    {
        return $this->categoryRepository->find($id);
    }

    public function delete($id)
    {
        $category = $this->categoryRepository->find($id);

        $category->delete();
        return $category;
    }

    public function update($request, $id)
    {
        $category = $this->categoryRepository->find($id);
        $dataUpdate = $request->all();

        $category->update($dataUpdate);
        return $category;
    }

    public function count()
    {
       return $this->categoryRepository->count();
    }
}
