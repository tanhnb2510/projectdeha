<?php

namespace Database\Seeders;

use App\Models\Permission;
use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SuperAdminSeeder extends Seeder
{
    public function run()
    {
        $user = User::create([
            'name' => 'Tuấn Anh',
            'email' => 'tanhtin@gmail.com',
            'birthday' => '2000/03/22',
            'address' => 'Ninh Binh',
            'phone_number' => '0975211645',
            'gender' => '0',
            'password' => bcrypt(12345678)
        ]);

        $role = Role::create([
            'name' => 'super_admin',
            'display_name' => 'SuperAdmin'
        ]);

        DB::table('role_user')->insert([
            'role_id' => $role->id,
            'user_id' => $user->id
        ]);

        $permissions = Permission::all();

        foreach ($permissions as $permission) {
            DB::table('role_permission')->insert([
                'permission_id' => $permission->id,
                'role_id' => $role->id
            ]);
        }
    }
}
