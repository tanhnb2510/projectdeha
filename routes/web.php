<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/home', function () {
    return view('admin.home.dashboard');
})->name('home');

Auth::routes();

//Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::prefix('roles')->name('roles.')->group(function(){
    Route::get('/', [\App\Http\Controllers\RoleController::class, 'index'])
        ->name('index')
        ->middleware('permission:role_view','auth');

    Route::get('/create', [\App\Http\Controllers\RoleController::class, 'create'])
        ->name('create')
        ->middleware('permission:role_create','auth');

    Route::post('/store', [\App\Http\Controllers\RoleController::class, 'store'])
        ->name('store')
        ->middleware('permission:role_store','auth');

    Route::get('/{id}', [\App\Http\Controllers\RoleController::class, 'show'])
        ->name('show')
        ->middleware('permission:role_show','auth');

    Route::get('/edit/{id}', [\App\Http\Controllers\RoleController::class, 'edit'])
        ->name('edit')
        ->middleware('permission:role_edit','auth');

    Route::put('/update/{id}', [\App\Http\Controllers\RoleController::class, 'update'])
        ->name('update')
        ->middleware('permission:role_update','auth');

    Route::delete('/delete/{id}', [\App\Http\Controllers\RoleController::class, 'destroy'])
        ->name('destroy')
        ->middleware('permission:role_delete','auth');
});



//-----------------------------User----------------------------------
Route::get('users/create', [\App\Http\Controllers\UserController::class, 'create'])
    ->name('users.create')
    ->middleware('permission:user_create','auth');

Route::get('users/{id}', [\App\Http\Controllers\UserController::class, 'show'])
    ->name('users.show')
    ->middleware('permission:user_show','auth');

Route::delete('users/delete/{id}', [\App\Http\Controllers\UserController::class, 'destroy'])
    ->name('users.destroy')
    ->middleware('permission:user_delete','auth');

Route::get('users/edit/{id}', [\App\Http\Controllers\UserController::class, 'edit'])
    ->name('users.edit')
    ->middleware('permission:user_edit','auth');

Route::post('users/store', [\App\Http\Controllers\UserController::class, 'store'])
    ->name('users.store')
    ->middleware('permission:user_store','auth');

Route::put('users/update/{id}',[\App\Http\Controllers\UserController::class,'update'])
    ->name('users.update')
    ->middleware('permission:user_update','auth');

Route::get('/user', [\App\Http\Controllers\UserController::class, 'index'])
    ->name('users.index')
    ->middleware('permission:user_view','auth');


// -------------------Category ----------------------------\\

Route::get('categories',[\App\Http\Controllers\CategoryController::class,'index'])
    ->name('categories.index')
    ->middleware('permission:category_view','auth');

Route::get('categorie/create',[\App\Http\Controllers\CategoryController::class,'create'])
    ->name('categories.create')
    ->middleware('permission:category_create','auth');

Route::post('categories/store',[\App\Http\Controllers\CategoryController::class,'store'])
    ->name('categories.store')
    ->middleware('permission:category_store','auth');

Route::get('categories/edit/{id}',[\App\Http\Controllers\CategoryController::class,'edit'])
    ->name('categories.edit')
    ->middleware('permission:category_edit','auth');

Route::put('categories/update/{id}',[\App\Http\Controllers\CategoryController::class,'update'])
    ->name('categories.update')
    ->middleware('permission:category_update','auth');

Route::delete('categories/delete/{id}',[\App\Http\Controllers\CategoryController::class,'destroy'])
    ->name('categories.destroy')
    ->middleware('permission:category_delete','auth');;

Route::get('categorie/list',[\App\Http\Controllers\CategoryController::class,'list'])
    ->name('categories.list')
    ->middleware('permission:category_view','auth');;

// -------------------Product ----------------------------\\

Route::get('products',[\App\Http\Controllers\ProductController::class,'index'])
    ->name('products.index');

Route::get('prodcuts/{id}',[\App\Http\Controllers\ProductController::class,'show'])
    ->name('products.show');

Route::get('products/create',[\App\Http\Controllers\ProductController::class,'create'])
    ->name('products.create');

Route::get('prodcuts/edit/{id}',[\App\Http\Controllers\ProductController::class,'edit'])
    ->name('products.edit');

Route::post('products/store',[\App\Http\Controllers\ProductController::class,'store'])
    ->name('products.store');

Route::put('prodcuts/update/{id}',[\App\Http\Controllers\ProductController::class,'update'])
    ->name('products.update');

Route::delete('products/delete/{id}',[\App\Http\Controllers\ProductController::class,'destroy'])
    ->name('products.destroy');

Route::get('products/list',[\App\Http\Controllers\ProductController::class,'list'])
    ->name('products.list');
