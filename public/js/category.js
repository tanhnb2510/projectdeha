$(".deleteCategory").click(function () {
    let name = $('.deleteCategory').data('name-show');
    return new Promise((resolve, reject) => {
        Swal.fire({
            title: 'Are you sure ' + name + ' ?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.isConfirmed) {
                resolve(true);
                let url =  $('.deleteCategory').data('action');
                Base.callApiNormally(url, {}, 'DELETE');
                alertMessage.alertSuccess('delete success');
            } else {
                reject(false);
            }
        })
    })
});

