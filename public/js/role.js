$(".dele-role-btn").click(function (){
    console.log(1);
    swal.fire({
        title: `Are you sure delete ?`,
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
    })
        .then((result)=>{
            if(result){
                event.preventDefault();
                $('#delete-role-form-'+$(this).data('id')).submit();
            }
        })
});
