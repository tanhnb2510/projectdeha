const Product = (function () {
    let modules = {};

    modules.resetForm = function (e) {
        e.trigger('reset');
        $(".avatar_show").attr("src", '');
    }

    modules.getList = function (url, data) {
        Base.callApiNormally(url, data)
            .then(function (res) {
                $('#list').html('').append(res);
            });
    }

    modules.getListByPageLink = function (e) {
        let url = e.attr('href');
            Product.getList(url);
    }

    modules.showEdit = function (e) {
        let url = e.data('action');
        Base.callApiData(url)
            .then(function (res) {
                $('#edit-product').html('').append(res);
            })
            .fail(function () {
                $('#exampleModalEdit').modal('hide');
            })
    };

    modules.showDetail = function (e) {
        let url = e.data('action');
        Base.callApiData(url)
            .then(function (res) {
                $('#detail-product').html('').append(res);
            })
            .fail(function () {
                $('#showProduct').modal('hide');
            })
    };

    modules.list = function () {
        let data = $('.form-search').serialize();
        let url = $('#list').data('action');
        Product.getList(url, data);
    };

    modules.showError = function (arrErrors) {
        for (let value in arrErrors) {
            $('.' + 'error-' + value).html(arrErrors[value][0]);
        }
    }

    modules.create = function () {
        let url = $('#addNewProductForm').data('action');
        let data = new FormData(document.getElementById('addNewProductForm'))
        Base.callApiData(url, data, 'POST')
            .then(function () {
                $('#exampleModal').modal('hide');
                Product.resetForm($('#addNewProductForm'));
                alertMessage.alertSuccess('create success');
                Product.getList($('#list').data('action'));
            })
            .catch(function (response) {
                Product.showError(response.responseJSON.errors);
            });
    };

    modules.edit = function () {
        let url = $('#editProductForm').data('action');
        let data = new FormData(document.getElementById('editProductForm'));
        data.append('_method', 'PUT')
        Base.callApiData(url, data, 'POST')
            .done(function (response) {
                $('#exampleModalEdit').modal('hide');
                alertMessage.alertSuccess('update success');
                Product.getList($('#list').data('action'));
            })
            .catch(function (response) {
                Product.showError(response.responseJSON.errors);
            });
    }

    function deleteproduct() {
        let url = $('#btn_delete').data('action');
        Base.callApiNormally(url, {}, 'DELETE')
            .done(function () {
                alertMessage.alertSuccess('delete success');
                Product.getList($('#list').data('action'));
            });
    }

    modules.delete = function () {
        modules.confirmDelete().then(deleteproduct);
    }
    modules.confirmDelete = function () {
        let name = $('#btn_delete').data('name-show');
        return new Promise((resolve, reject) => {
            Swal.fire({
                title: 'Are you sure ' + name + ' ?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.isConfirmed) {
                    resolve(true);
                } else {
                    reject(false);
                }
            })
        })
    }

    return modules;
}(window.jQuery, window, document));

$(document).ready(function () {

    Product.getList($('#list').data('action'));

    $(document).on('click', '.page-link', function (e) {
        e.preventDefault();
        Product.getListByPageLink($(this));
    });

    $(document).on('click', '#btn-create-product', function (e) {
        e.preventDefault();
        alertMessage.resetError();
        Product.create();
    })

    $(document).on('click', '#btn-edit-product', function (e) {
        e.preventDefault();
        alertMessage.resetError();
        Product.edit();
    })
    $(document).on('click', '#btn-open-edit', function () {
        Product.showEdit($(this));
    })

    $(document).on('click', '#btn_delete', function () {
        Product.delete($(this));
    })

    $(document).on('click', '.btn-create-new-product', function () {
        alertMessage.resetError();
        loadImage.deleteImage();
    })

    $(document).on('click', '#btn-open-detail', function () {
        Product.showDetail($(this));
    })

    $('#category_search').on('change', debounce(function () {
        Product.list();
    }))

    $('#name-search,.min-price-search,.max-price-search').on('keyup', debounce(function () {
        Product.list();
    }))
})

function debounce(func, timeout = 300) {
    let timer;
    return (...args) => {
        clearTimeout(timer);
        timer = setTimeout(() => {
            func.apply(this, args);
        }, timeout);
    };
}
